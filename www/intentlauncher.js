/*global cordova, module*/

module.exports = {
    startCustomIntent: function (params,successCallback, errorCallback) {
		
		if(!successCallback){successCallback=function(){}}
		if(!errorCallback){errorCallback=function(){}}
		
        cordova.exec(successCallback, errorCallback, "Intentlauncher", "startCustomIntent", [params]);
    }
};
