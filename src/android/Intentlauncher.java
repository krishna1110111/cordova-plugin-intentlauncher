package com.kavworks.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ActionBar;
import android.widget.Toast;
import android.view.View;
import android.content.Intent;
import android.content.ComponentName;
import java.util.Map;
import java.util.HashMap;

public class Intentlauncher extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if(action.equals("startCustomIntent")){
            if (args.length() != 1) {
                //return new PluginResult(PluginResult.Status.INVALID_ACTION);
                callbackContext.success("failed");
                return false;
            }


            JSONObject obj = args.getJSONObject(0);


            JSONObject extras = obj.has("extras") ? obj.getJSONObject("extras") : null;
            Map<String, String> extrasMap = new HashMap<String, String>();

            // Populate the extras if any exist
            if (extras != null) {
                JSONArray extraNames = extras.names();
                for (int i = 0; i < extraNames.length(); i++) {
                    String key = extraNames.getString(i);
                    String value = extras.getString(key);
                    extrasMap.put(key, value);
                }
            }

            startNewCustomIntent(obj.getString("pack"), extrasMap);
            callbackContext.success("succeeded");
            return true;



        }

        return false;
    }

    void startNewCustomIntent(String pack, Map<String, String> extras){

        Intent intent = new Intent();
        intent.setComponent(new ComponentName(pack, pack+".MainActivity"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        for (String key : extras.keySet()) {
            String value = extras.get(key);
            intent.putExtra(key, value);
        }

        ((CordovaActivity)this.cordova.getActivity()).startActivity(intent);
    }

}